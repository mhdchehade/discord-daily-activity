from datetime import date
from datetime import timedelta
import datetime
import re
import m_email
import sys
import compress_image

# The public plotly graphs to include in the report. These can also be generated with `py.plot(figure, filename)`
graphs = []

_report_desc = ''
t_date = datetime.datetime.utcnow()
yesterday = t_date - timedelta(days = 1)
morning =  yesterday.replace(hour=0, minute=0, second=0, microsecond=0)
evening =  yesterday.replace(hour=23, minute=59, second=59, microsecond=0)
images_path = 'images'

f = open("images_channel", "r")
IMAGE_CHANNEL = f.read()
IMAGE_CHANNEL = int(IMAGE_CHANNEL[:-1])

def update_time(days_adjust = 0):
    global t_date
    global yesterday
    global morning
    global evening
    global _report_desc
    t_date = datetime.datetime.utcnow()
    yesterday = t_date - timedelta(days = (1 + days_adjust))
    morning =  yesterday.replace(hour=0, minute=0, second=0, microsecond=0)
    evening =  yesterday.replace(hour=23, minute=59, second=59, microsecond=0)

    _report_desc = ('' +
    '<!DOCTYPE html>' +
    '<title>Daily Report</title>' +

    '<style>' +
    '  img {' +
    '    border: 2px solid black;' +
    '  }' +
    '</style>' +
    '<h1 style="font-size:150%; color:Tomato; font-family:verdana;">Daily Activity for ' + yesterday.strftime("%A %d/%m/%Y") +
    ':</h1>'
    )

def report_block_template(caption, item_n, graph_url=[]):
    graph_block = ''
    for graph in graph_url:
        graph_block += (''
            '<a href="{graph_url}" target="_blank">' # Open the interactive graph when you click on the image
                '<img style="height: 400px;" src="{graph_url}">'
            '</a><br><br>').format(graph_url=graph)

    report_block = ('' +
        '<p style="font-size:130%;">{item_n} - ' + '{caption}</p>' + # Optional caption to include below the graph
        '<br>'      + # Line break
        graph_block +
        '<hr><br>') # horizontal line

    return report_block.format(  item_n =item_n + 1, caption=caption)



from xhtml2pdf import pisa             # import python module

static_report = ''
# Utility function
def convert_html_to_pdf(source_html, output_filename):

    # open output file for writing (truncated binary)
    result_file = open(output_filename, "w+b")

    # convert HTML to PDF
    pisa_status = pisa.CreatePDF(
            source_html,                # the HTML to convert
            dest=result_file)           # file handle to recieve result

    # close output file
    result_file.close()                 # close output file

    # return True on success and False on errors
    return pisa_status.err


# discord
import discord
from discord.ext import commands

PRE = ['$']
DESC = "Daily activity reporter"
bot = commands.Bot(command_prefix=PRE, description = DESC)

@bot.command()
async def reboot(ctx):
    print('Exiting')
    sys.exit(0)
    #os.excel("restart.sh","")

@bot.command()
async def report(ctx):
    print('report starting')
    global graphs
    global static_report
    message_content = ctx.message.content.split()
    if 'tb' in ctx.message.content:
        for i in range(len(message_content)):
            if message_content[i] == 'tb':
                update_time(int(message_content[i+1]))
    else:
        update_time()
    static_report = ''
    static_report += _report_desc
    #m_messages = ''

    daily_activity_channel = bot.get_channel(770575973468078090)
    if 'ts' in ctx.message.content:
        for i in range(len(message_content)):
            if message_content[i] == 'ts':
                m_ts = int(message_content[i+1])
                m_messages = await daily_activity_channel.history(before=(evening - timedelta(days = m_ts) ), after=(morning  - timedelta(days = m_ts)), oldest_first=True).flatten()
    else:
        m_messages = await daily_activity_channel.history(before=(evening - timedelta(days = 0) ), after=(morning  - timedelta(days = 0)), oldest_first=True).flatten()

    for r_message in m_messages:
        cnt = r_message.content
        if not '$' in cnt:
            graphs.append(cnt.split('\n'))


    for i in range(len(graphs)):
        _static_block = report_block_template(graphs[i][0], i, graphs[i][1:])

        static_report += _static_block
    convert_html_to_pdf(static_report, 'report.pdf')
    print(f'sending static report for {yesterday}')
    m_email.send_email()
    graphs = []

@bot.event
async def on_message(message):
    await bot.process_commands(message)

    if not message.channel.id == IMAGE_CHANNEL:
        return
    print('resizing image')
    #if not message.channel.id == IMAGE_CHANNEL:
    #    return
    if message.author.bot:
        return
    m_attachments = message.attachments
    if len(m_attachments) == 0:
        return

    i = 0

    for att in m_attachments:
        await att.save(f"{images_path}/image_{i}.jpg")
        compress_image.compress(f"{images_path}/image_{i}.jpg", 20)
        await message.channel.send(file=discord.File(f"{images_path}/image_{i}.jpg"))
        i += 1

f = open("token", "r")
TOKEN = f.read()
TOKEN = TOKEN[:-1]

bot.run(TOKEN)
# discord finsih
