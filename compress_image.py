# whatsapp like image compression

import time
from subprocess import call


def compress(image_path, newSize):
    call(["convert",f"{image_path}","-resize",f"{newSize}%", "-quality", "50", f"{image_path}"])
