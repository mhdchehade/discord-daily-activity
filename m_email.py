import smtplib, ssl, time
import email

from datetime import date
from datetime import timedelta
import datetime
import re


from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

def send_email():
    m_time = datetime.datetime.utcnow()
    m_yesterday = m_time - timedelta(days = 1)

    f = open("password", "r")
    password = f.read()
    password = password[:-1]

    f = open("sendermail", "r")
    sender_email = f.read()
    sender_email = sender_email[:-1]

    f = open("receivermail", "r")
    receiver_email = f.read()
    receiver_email = receiver_email[:-1]

    port = 587  # For starttls
    smtp_server = "smtp.gmail.com"

    subject = "Daily report"
    body = "Dear Mr. Abdulhafiz, \n Attached is my daily activities report for " + m_yesterday.strftime("%A %d/%m/%Y")  + '.\nBest regards,\nMohamad Faris.'

    # Create a multipart message and set headers
    message = MIMEMultipart()
    message["From"] = sender_email
    message["To"] = receiver_email
    message["Subject"] = subject
    message["Bcc"] = receiver_email  # Recommended for mass emails

    # Add body to email
    message.attach(MIMEText(body, "plain"))

    filename = "report.pdf"  # In same directory as script

    # Open PDF file in binary mode
    with open(filename, "rb") as attachment:
        # Add file as application/octet-stream
        # Email client can usually download this automatically as attachment
        part = MIMEBase("application", "octet-stream")
        part.set_payload(attachment.read())

    # Encode file in ASCII characters to send by email
    encoders.encode_base64(part)

    # Add header as key/value pair to attachment part
    part.add_header(
        "Content-Disposition",
        f"attachment; filename= {filename}",
    )

    # Add attachment to message and convert message to string
    message.attach(part)
    text = message.as_string()

    context = ssl.create_default_context()
    with smtplib.SMTP(smtp_server, port) as server:
        server.ehlo()  # Can be omitted
        server.starttls(context=context)
        server.ehlo()  # Can be omitted
        time.sleep(1)
        server.login(sender_email, password)
        time.sleep(1)
        server.sendmail(sender_email, receiver_email, text)
